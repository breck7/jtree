const GrammarBackedNode = require("./GrammarBackedNode.js")

class GrammarBackedTerminalNode extends GrammarBackedNode {}

module.exports = GrammarBackedTerminalNode
