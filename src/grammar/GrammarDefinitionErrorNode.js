const TreeNode = require("../base/TreeNode.js")

class GrammarDefinitionErrorNode extends TreeNode {
  getErrors() {
    return [`unknownKeywordError "${this.getKeyword()}" at line ${this.getPoint().y}`]
  }

  getLineSyntax() {
    return ["keyword"].concat(this.getWordsFrom(1).map(word => "any")).join(" ")
  }
}

module.exports = GrammarDefinitionErrorNode

